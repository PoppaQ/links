# :cloud: Resources

### Serverless Computing

[https://cloud.google.com/serverless/whitepaper/](https://cloud.google.com/serverless/whitepaper/)

[https://hackernoon.com/cloud-migration-checklist-step-by-step-guide-e65839cb2e81](https://hackernoon.com/cloud-migration-checklist-step-by-step-guide-e65839cb2e81)

[https://www.youtube.com/watch?v=JenJQ6gc14U&index=134&list=PLBgogxgQVM9v0xG0QTFQ5PTbNrj8uGSS-&t=0s](https://www.youtube.com/watch?v=JenJQ6gc14U&index=134&list=PLBgogxgQVM9v0xG0QTFQ5PTbNrj8uGSS-&t=0s)


### Continuous Delivery

[https://aws.amazon.com/devops/continuous-delivery/](https://aws.amazon.com/devops/continuous-delivery/)

[https://circleci.com/customers/sony/](https://circleci.com/customers/sony/)

[https://www.digitalocean.com/community/tutorials/an-introduction-to-continuous-integration-delivery-and-deployment](https://www.digitalocean.com/community/tutorials/an-introduction-to-continuous-integration-delivery-and-deployment)


### Testing

[https://techbeacon.com/6-best-practices-integration-testing-ci-environment](https://techbeacon.com/6-best-practices-integration-testing-ci-environment)

[https://saucelabs.com/blog/testing-in-continuous-delivery-shift-left](https://saucelabs.com/blog/testing-in-continuous-delivery-shift-left)

[https://www.youtube.com/watch?v=4ATMFFHb4NU&index=12&list=PLBgogxgQVM9v0xG0QTFQ5PTbNrj8uGSS-&t=0s](https://www.youtube.com/watch?v=4ATMFFHb4NU&index=12&list=PLBgogxgQVM9v0xG0QTFQ5PTbNrj8uGSS-&t=0s)

